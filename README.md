# The Unlikely Orange Conduit Conundrum

This is the development space for the Linux Game Jam and Adventure Game Jam project of Jigme Datse Yli-Rasku

## License

This is currently using GPL 3.0 ...  Might not be the "best license" for whatever reason, but GPL is the license I am most familiar with and it seems that FSF considers that it is fair.  It is GPL 3.0 or later I guess.  

